#include <QApplication>
#include <QtUiTools>
#include <string>
#include "mainscreen.h"
#include "musicscreen.h"
#include "mapsscreen.h"
#include "logscreen.h"
#include "navigationhud.h"
#include "mpd.h"
#include <iostream>
#include "ui_music.h"
#include "notifications.h"
#include "timersystem.h"
#include "arch.h"

MainScreen* mainScreen;
MusicScreen* musicScreen;
MapsScreen* mapsScreen;
LogScreen* logScreen;
QWidget* mainWindow;
NavigationHud* navigationHud;
Notifications* notifications;

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    #ifdef DESKTOP
        mainWindow = new QWidget();
        mainWindow->resize(800, 480);
        mainWindow->show();
    #endif
	
	logScreen = new LogScreen();
	
	Mpd.Connect();

    notifications = new Notifications();
	navigationHud = new NavigationHud();
    musicScreen = new MusicScreen();
    mapsScreen = new MapsScreen();
    mainScreen = new MainScreen();
    
    ScreenSystem::SwitchTo(mainScreen);
	
    LogW("All", "Logging Started");
	
    mapsScreen->InitGPS();
	
	TimerSystem::Start();
	
    return app.exec();
}
