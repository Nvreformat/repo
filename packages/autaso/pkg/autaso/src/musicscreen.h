#pragma once
#include "basescreen.h"
#include "ui_music.h"
#include <string>
#include <map>
#include <vector>
#include "mpd.h"

class MusicScreen : public BaseScreen
{
	Q_OBJECT
	
	Ui::MusicForm ui;
	QStandardItemModel* libraryModel;
	QStandardItemModel* searchModel;
	QStandardItemModel* playlistModel;
	std::map<std::string, QStandardItem*> itemCache;
    std::vector<MPD::Song> searchResults;
    int lastSearchSize = 100;
	
	void SetCurrentButton(QPushButton* button);
    void AddItem(MPD::Song song, QStandardItemModel* root, bool makeFolders);
	
	public:
		QIcon icon1;
		QIcon icon2;
		QStandardItem* item0;
		
		void SwitchTo();
		
		MusicScreen();
		
	public slots:
		void OnItemClick(const QModelIndex&);
		void ExpandItem(const QModelIndex &index);
		void OnSideButtonClick();
        void OnSearchType();
};

