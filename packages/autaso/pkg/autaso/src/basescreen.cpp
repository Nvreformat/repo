#include <QApplication>
#include <QtUiTools>
#include <string>
#include "basescreen.h"
#include "navigationhud.h"
#include "arch.h"

using namespace std;

void BaseScreen::SwitchTo()
{
	SwitchTo(true);
}

void BaseScreen::SwitchTo(bool left)
{
	ScreenSystem::SwitchTo(this, left);
	
	navigationHud->SetBottomBarVisible(false);
}

void BaseScreen::Setup(QWidget* widget)
{
	if (fullScreen)
		positionOffset = 0;
	else
		positionOffset = 41;
	
	baseWidget = widget;
	baseWidget->setWindowFlags(Qt::Widget | Qt::FramelessWindowHint);
	baseWidget->move(0, positionOffset);
		
	animation = new QPropertyAnimation(baseWidget, "geometry");
	
	this->connect(animation, SIGNAL(finished()), this, SLOT(OnAnimationFinished()));
    baseWidget->hide();
    #ifdef DESKTOP
        baseWidget->setParent(mainWindow);
    #endif
}

void BaseScreen::SetFullScreen(bool fullScreen)
{
	this->fullScreen = fullScreen;
}

void BaseScreen::OnAnimationFinished()
{
	if (ScreenSystem::currentScreen != this)
	{
		baseWidget->hide();
		
		ScreenSystem::switchingScreen = false;
	}
}
	
void BaseScreen::SetVisible(bool visible, bool animate)
{
	if (animate)
	{
		animation->setDuration(300);
		animation->setEasingCurve(QEasingCurve::OutQuad);
		
		if (ScreenSystem::switchingLeft)
		{
			if (visible)
			{
				animation->setStartValue(QRect(baseWidget->width(), positionOffset, baseWidget->width(), baseWidget->height()));
				animation->setEndValue(QRect(0, positionOffset, baseWidget->width(), baseWidget->height()));
			}
			else
			{
				animation->setStartValue(QRect(0, positionOffset, baseWidget->width(), baseWidget->height()));
				animation->setEndValue(QRect(-baseWidget->width(), positionOffset, baseWidget->width(), baseWidget->height()));
			}
		}
		else
		{
			if (visible)
			{
				animation->setStartValue(QRect(-baseWidget->width(), positionOffset, baseWidget->width(), baseWidget->height()));
				animation->setEndValue(QRect(0, positionOffset, baseWidget->width(), baseWidget->height()));
			}
			else
			{
				animation->setStartValue(QRect(0, positionOffset, baseWidget->width(), baseWidget->height()));
				animation->setEndValue(QRect(baseWidget->width(), positionOffset, baseWidget->width(), baseWidget->height()));
			}
		}
			
		animation->start();
		ScreenSystem::switchingScreen = true;
	}

	baseWidget->show();
}

BaseScreen* ScreenSystem::currentScreen = 0;
bool ScreenSystem::switchingScreen = false;
bool ScreenSystem::switchingLeft = false;

void ScreenSystem::PreviousScreen()
{
	if ((void*)currentScreen != (void*)mainScreen)
		currentScreen->previousScreen->SwitchTo(false);
}

void ScreenSystem::SwitchTo(BaseScreen* screen)
{
	SwitchTo(screen, true);
}

void ScreenSystem::SwitchTo(BaseScreen* screen, bool left)
{
	if (ScreenSystem::switchingScreen)
		return;
	
	switchingLeft = left;
	
	if (currentScreen != 0)
	{
		screen->SetVisible(true, true);
		currentScreen->SetVisible(false, true);
	}
	else
	{
		screen->SetVisible(true, false);
	}
	
	screen->previousScreen = currentScreen;
	currentScreen = screen;
}
