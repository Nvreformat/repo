#pragma once
#include <QApplication>
#include <QtUiTools>
#include <QWidget>
#include <string>

class BaseScreen : public QObject
{
	Q_OBJECT
	
	QPropertyAnimation* animation;
	QWidget* baseWidget;
	bool fullScreen = false;
	int positionOffset;
	
	public:
		BaseScreen* previousScreen = 0;
	
		void Setup(QWidget* widget);
		void SetVisible(bool visible, bool animate);
		void SwitchTo();
		void SwitchTo(bool);
		void SetFullScreen(bool fullScreen);
		
	private slots:
		void OnAnimationFinished();
};

namespace ScreenSystem
{
	extern BaseScreen* currentScreen;
	extern bool switchingScreen;
	extern bool switchingLeft;
	
	void SwitchTo(BaseScreen* screen, bool left);
	void SwitchTo(BaseScreen* screen);
	void PreviousScreen();
}

class MusicScreen;
class MainScreen;
class MapsScreen;
class NavigationHud;
class LogScreen;

extern QWidget* mainWindow;
extern MainScreen* mainScreen;
extern MusicScreen* musicScreen;
extern MapsScreen* mapsScreen;
extern LogScreen* logScreen;
extern NavigationHud* navigationHud;


