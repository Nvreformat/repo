#pragma once

#include <functional>

namespace TimerSystem
{ 
	void Start();
	void AddTimer(std::function<void(void)> function, int priority);
}
