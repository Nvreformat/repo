#include "basescreen.h"
#include "ui_maps.h"
#include <QWebFrame>
#include <gps.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include <string>
#include "logscreen.h"

class MapsScreen : public BaseScreen
{
	Q_OBJECT
	
	Ui::MapsForm ui;
	
	public:
	
	gps_data_t gpsData;
	bool gpsOK = false;
	
	void InitGPS();
	MapsScreen();
	
	private slots:
		void OnTimerTick();
};
