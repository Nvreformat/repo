#include "navigationhud.h"
#include "basescreen.h"
#include "mpd.h"
#include <stdio.h>
#include <time.h>
#include <string>
#include "notifications.h"
#include "timersystem.h"
#include "arch.h"

NavigationHud::NavigationHud()
{
	widgetTop = new QWidget;	
	uiTop.setupUi(widgetTop);	
	widgetTop->setWindowFlags(Qt::Widget | Qt::FramelessWindowHint);
    widgetTop->move(0, 0);
    #ifdef DESKTOP
        widgetTop->setParent(mainWindow);
    #endif
	widgetTop->show();
	
	widgetBottom = new QWidget;	
	uiBottom.setupUi(widgetBottom);	
	widgetBottom->setWindowFlags(Qt::Widget | Qt::FramelessWindowHint);
    widgetBottom->move(0, 480);
    #ifdef DESKTOP
        widgetBottom->setParent(mainWindow);
    #endif
    widgetBottom->show();

	TimerSystem::AddTimer(std::bind(&NavigationHud::OnTimerTick, this), 1);
    
    animation = new QPropertyAnimation(widgetBottom, "geometry");
	
	animation->setDuration(300);
	animation->setEasingCurve(QEasingCurve::OutQuad);
		
	playIcon.addFile(QStringLiteral(":/res/play.png"), QSize(), QIcon::Normal, QIcon::On);
	pauseIcon.addFile(QStringLiteral(":/res/pause.png"), QSize(), QIcon::Normal, QIcon::On);
	
	this->connect(uiTop.BackButton, SIGNAL(clicked()), this, SLOT(OnBackClick()));
	this->connect(uiTop.PlayPauseButton, SIGNAL(clicked()), this, SLOT(OnTogglePauseClick()));
	this->connect(uiTop.NextButton, SIGNAL(clicked()), this, SLOT(OnNextClick()));
	this->connect(uiTop.PreviousButton, SIGNAL(clicked()), this, SLOT(OnPreviousClick()));
	this->connect(uiTop.VolDownButton, SIGNAL(clicked()), this, SLOT(OnVoldownClick()));
	this->connect(uiTop.VolUpButton, SIGNAL(clicked()), this, SLOT(OnVolupClick()));
	
	OnTimerTick();
}

void NavigationHud::SetBottomBarVisible(bool visible)
{
	if (bottomBarVisible == visible)
		return;
	
	if (visible)
	{
		//widgetBottom->move(0, 449);
		animation->setStartValue(QRect(0, 480, widgetBottom->width(), widgetBottom->height()));
		animation->setEndValue(QRect(0, 449, widgetBottom->width(), widgetBottom->height()));
	}
	else
	{
		//widgetBottom->move(0, 480);
		animation->setStartValue(QRect(0, 449, widgetBottom->width(), widgetBottom->height()));
		animation->setEndValue(QRect(0, 480, widgetBottom->width(), widgetBottom->height()));
	}
	
	animation->start();
	bottomBarVisible = visible;
	widgetBottom->activateWindow();
	widgetBottom->raise();
}

void NavigationHud::OnTimerTick()
{
	// Clock
	static std::string lastSong = "!";
	static bool showColon = false;
	
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	char buffer[8];
	char buffer2[8];
	
	std::snprintf(buffer, sizeof(buffer), "%02i", tm.tm_hour);
	uiTop.HourLabel->setText(buffer);
	std::snprintf(buffer, sizeof(buffer), "%02i", tm.tm_min);
	uiTop.MinuteLabel->setText(buffer);
	showColon ? uiTop.ColonLabel->show() : uiTop.ColonLabel->hide();

	showColon = !showColon;
	
	if (Mpd.getStatus().playerState() == MPD::psPlay)
		uiTop.PlayPauseButton->setIcon(pauseIcon);
	else
		uiTop.PlayPauseButton->setIcon(playIcon);
		
	if (Mpd.Connected() && !Mpd.GetCurrentSong().empty())
	{
        std::string visibleStr = Mpd.GetSongString(Mpd.GetCurrentSong(), true);
		
		uiBottom.SongNameLabel->setText(visibleStr.c_str());
		
		int elapsedTimeRaw = Mpd.getStatus().elapsedTime();
		int totalTimeRaw = Mpd.getStatus().totalTime();
		int elapsedTimeSecs = elapsedTimeRaw % 60;
		int elapsedTimeMins = elapsedTimeRaw / 60;
		int totalTimeSecs = totalTimeRaw % 60;
		int totalTimeMins = totalTimeRaw / 60;
		
        if (elapsedTimeSecs < 2 && elapsedTimeMins == 0 && !IsBottomBarVisible() && Mpd.getStatus().playerState() == MPD::psPlay)
			notifications->Put(visibleStr, 5000);
		
		std::snprintf(buffer, sizeof(buffer), "%02i", elapsedTimeSecs);
		std::snprintf(buffer2, sizeof(buffer2), "%02i", totalTimeSecs);
		
		std::string str = std::string("[") + std::to_string(elapsedTimeMins) + ":" + buffer + "/" + std::to_string(totalTimeMins) + ":" + buffer2 + "]";
		uiBottom.SongProgressLabel->setText(str.c_str());
		uiBottom.ProgressBar->resize(((float)elapsedTimeRaw / totalTimeRaw) * 800, uiBottom.ProgressBar->height());
	}
}

void NavigationHud::OnTogglePauseClick()
{
	if (Mpd.Connected())
		Mpd.Toggle();
}

void NavigationHud::OnNextClick()
{
	if (Mpd.Connected())
	{
		Mpd.Next();
		Mpd.Play();
	}
}

void NavigationHud::OnPreviousClick()
{
	if (Mpd.Connected())
	{
		Mpd.Prev();
		Mpd.Play();
	}
}

void NavigationHud::OnVolupClick()
{
	if (Mpd.Connected())
	{
		int volume = Mpd.getStatus().volume();
		
		if (volume <= 95)
			volume += 5;
		else
			volume = 100;
		
		Mpd.SetVolume(volume);
		notifications->Put("Volumen: " + std::to_string(volume / 5));
	}
}

void NavigationHud::OnVoldownClick()
{
	if (Mpd.Connected())
	{
		int volume = Mpd.getStatus().volume();
		
		if (volume >= 5)
			volume -= 5;
		else
			volume = 0;
		
		Mpd.SetVolume(volume);
		notifications->Put("Volumen: " + std::to_string(volume / 5));
	}
}

void NavigationHud::OnBackClick()
{
	ScreenSystem::PreviousScreen();
}
