#include "musicscreen.h"
#include <ftw.h>
#include <stdio.h>
#include <sys/stat.h>
#include <string>
#include <libgen.h>
#include <iostream>
#include <sstream>
#include <vector>
#include "mpd.h"
#include "navigationhud.h"

using namespace std;

void split(const string &s, char delim, vector<string> &elems) {
    stringstream ss;
    ss.str(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
}

vector<string> split(const string &s, char delim) {
    vector<string> elems;
    split(s, delim, elems);
    return elems;
}

Q_DECLARE_METATYPE(string)
void MusicScreen::OnItemClick(const QModelIndex& item)
{
	string value = item.data(Qt::UserRole + 1).value<string>();
	
	if (!value.empty())
	{
		if (Mpd.Connected())
		{
			MPD::Song song = Mpd.GetSong(value);
			
			if (!song.getID())
				Mpd.PlayID(Mpd.AddSong(song));
			else
				Mpd.PlayID(song.getID());
		}
	}
		
}

void MusicScreen::ExpandItem(const QModelIndex &index)
{
    ui.songsTreeView->isExpanded(index)? ui.songsTreeView->collapse(index) : ui.songsTreeView->expand(index);
}

void MusicScreen::AddItem(MPD::Song song, QStandardItemModel* root, bool makeFolders)
{
    string uri = song.getURI();
	vector<string> elements = split(uri, '/');
    string songString = Mpd.GetSongString(song, !makeFolders);

	if (elements.size() > 1)
	{
		unsigned int foldersTraversed = 0;
		string relPath;
		QStandardItem* entry = nullptr;
		QStandardItem* lastEntry = nullptr;
		
		while(foldersTraversed != elements.size() - 1)
		{
			entry = itemCache[relPath + elements[foldersTraversed]];
				
			if (!entry)
			{
				entry = new QStandardItem(musicScreen->icon2, elements[foldersTraversed].c_str());
				itemCache[relPath + elements[foldersTraversed]] = entry;
				
				if (makeFolders)
					if (!lastEntry)
						root->appendRow(entry);
					else
						lastEntry->appendRow(entry);
			}
				
			relPath = relPath + elements[foldersTraversed]  + "/";
			foldersTraversed++;
			lastEntry = entry;
		}
		
        QStandardItem* newItem = new QStandardItem(musicScreen->icon1, QString::fromUtf8(songString.data(), songString.size()));
		newItem->setData(QVariant::fromValue(uri));
		
		if (makeFolders)
			entry->appendRow(newItem);
		else
			root->appendRow(newItem);
	}
	else if (!makeFolders)
	{
        QStandardItem* newItem = new QStandardItem(musicScreen->icon1, QString::fromUtf8(songString.data(), songString.size()));
		newItem->setData(QVariant::fromValue(uri));
        root->appendRow(newItem);
	}
}

void MusicScreen::SwitchTo()
{
	ScreenSystem::SwitchTo(this);
	
	navigationHud->SetBottomBarVisible(true);
}

void MusicScreen::SetCurrentButton(QPushButton* button)
{
	ui.searchButton->setChecked(false);
	ui.libraryButton->setChecked(false);
	ui.configButton->setChecked(false);
	ui.playlistButton->setChecked(false);
	button->setChecked(true);
	
	if (button == ui.playlistButton)
	{
		ui.songsTreeView->show();
		ui.searchEdit->hide();
		ui.songsTreeView->move(100, 0);
		ui.songsTreeView->resize(700, 410);
		ui.songsTreeView->setModel(playlistModel);
		
		playlistModel->clear();
		
		if (Mpd.Connected())
		{
			for (MPD::SongIterator s = Mpd.GetPlaylistChanges(0), end; s != end; ++s)
			{
                //itemCache.clear();
                AddItem(*s, playlistModel, false);
			}
		}
	}
	else if (button == ui.libraryButton)
	{
		ui.songsTreeView->show();
		ui.searchEdit->hide();
		ui.songsTreeView->move(100, 0);
		ui.songsTreeView->resize(700, 410);
		ui.songsTreeView->setModel(libraryModel);
	}
	else if (button == ui.searchButton)
	{
		ui.songsTreeView->show();
		ui.searchEdit->show();
		ui.songsTreeView->move(100, 30);
		ui.songsTreeView->resize(700, 380);
        ui.songsTreeView->setModel(searchModel);
        ui.searchEdit->setFocus();
	}
	else if (button == ui.configButton)
	{
		ui.songsTreeView->hide();
		ui.searchEdit->hide();
	}
}

void MusicScreen::OnSideButtonClick()
{
	QPushButton* button = (QPushButton*)sender();
	
	SetCurrentButton(button);
}

string upper(string val)
{
   transform(val.begin(), val.end(), val.begin(),::toupper);

   return val;
}

void MusicScreen::OnSearchType()
{
    searchModel->clear();
    string text = string(ui.searchEdit->text().toUtf8().constData());

    if (!ui.searchButton->isChecked())
        return;

    if (text.length() < 3)
        return;

    text = upper(text);

    if (text.length() == 3 or text.length() < lastSearchSize)
    {
        cout << "text" << endl;
        searchResults.clear();

        for (MPD::SongIterator s = Mpd.GetDirectoryRecursive("/"), end; s != end; ++s)
        {
            searchResults.push_back(*s);
        }
    }

    lastSearchSize = text.length();

    vector<MPD::Song> newResults;

    for (MPD::Song& s : searchResults)
    {
        if ((upper(s.getAlbum()).find(text) != std::string::npos) or
            (upper(s.getAlbumArtist()).find(text) != std::string::npos) or
            (upper(s.getArtist()).find(text) != std::string::npos) or
            (upper(s.getComposer()).find(text) != std::string::npos) or
            (upper(s.getName()).find(text) != std::string::npos) or
            (upper(s.getTitle()).find(text) != std::string::npos) or
            (upper(s.getPerformer()).find(text) != std::string::npos) or
            (upper(s.getTrack()).find(text) != std::string::npos))
        {
            AddItem(s, searchModel, false);
            newResults.push_back(s);
        }
    }

    searchResults = newResults;
}

MusicScreen::MusicScreen()
{
	musicScreen = this;
    QWidget *widget = new QWidget;
	ui.setupUi(widget);
	
	libraryModel = new QStandardItemModel();
	searchModel = new QStandardItemModel();
	playlistModel = new QStandardItemModel();
		
	QFont fnt;
	fnt.setPixelSize(20);
	ui.songsTreeView->setFont(fnt);
	ui.songsTreeView->setIconSize(QSize(40, 40));
	ui.songsTreeView->setModel(libraryModel);
	ui.songsTreeView->setEditTriggers(QAbstractItemView::NoEditTriggers);
	QObject::connect(ui.songsTreeView, SIGNAL(clicked(const QModelIndex&)), musicScreen, SLOT(OnItemClick(const QModelIndex&)));
	QObject::connect(ui.playlistButton, SIGNAL(clicked()), this, SLOT(OnSideButtonClick()));
	QObject::connect(ui.searchButton, SIGNAL(clicked()), this, SLOT(OnSideButtonClick()));
	QObject::connect(ui.libraryButton, SIGNAL(clicked()), this, SLOT(OnSideButtonClick()));
	QObject::connect(ui.configButton, SIGNAL(clicked()), this, SLOT(OnSideButtonClick()));
    QObject::connect(ui.songsTreeView, SIGNAL(clicked(const QModelIndex &)), this, SLOT(ExpandItem(const QModelIndex &)));
    QObject::connect(ui.searchEdit, SIGNAL(textChanged(QString)), this, SLOT(OnSearchType()));
	QScroller::grabGesture(ui.songsTreeView, QScroller::LeftMouseButtonGesture);
	ui.songsTreeView->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
	ui.songsTreeView->setAttribute(Qt::WA_AcceptTouchEvents, false);
	icon1 = QIcon(":/res/song.png");
	icon2 = QIcon(":/res/folder.png");
	
	if (Mpd.Connected())
	{
		for (MPD::SongIterator s = Mpd.GetDirectoryRecursive("/"), end; s != end; ++s)
		{
			//cout << s->getURI() << endl;
            AddItem(*s, libraryModel, true);
		}
	}
		
	Setup(widget);
	
	SetCurrentButton(ui.libraryButton);
}

