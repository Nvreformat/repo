#include "notifications.h"
#include "basescreen.h"
#include <QApplication>
#include <QtUiTools>
#include <iostream>
#include "arch.h"

using namespace std;

void Notifications::Close()
{
	anim->setStartValue(1);
	anim->setEndValue(0);
	anim->start();
	mustClose = true;
}

void Notifications::AnimFinished()
{
	if (mustClose)
		notificationLabel->hide();
	
	mustClose = false;
}
	
Notifications::Notifications()
{
    #ifdef DESKTOP
        notificationLabel = new QLabel(mainWindow);
    #else
        notificationLabel = new QLabel();
    #endif
    notificationLabel->setStyleSheet(QLatin1String("color: white;\n"
	"qproperty-alignment: AlignCenter;\n"
    "background-color: rgba(0, 0, 0, 200); border-radius: 5px;\n"
	"border: 2px solid rgb(100, 100, 100);\n"
	"color: white;\n"
    "font-size: 16pt;"));
    //notificationLabel->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
    //notificationLabel->setAttribute(Qt::WA_TranslucentBackground, true);
    //notificationLabel->setAttribute(Qt::WA_NoSystemBackground, true);
	
	QGraphicsOpacityEffect *eff = new QGraphicsOpacityEffect(this);
	notificationLabel->setGraphicsEffect(eff);
	anim = new QPropertyAnimation(eff,"opacity");
    anim->setDuration(250);
	anim->setEasingCurve(QEasingCurve::OutQuad);
	anim->setStartValue(0);
	anim->setEndValue(1);
	anim->start();
	
	connect(anim , SIGNAL(finished()), this, SLOT(AnimFinished()));
}
	
void Notifications::Put(string message, int duration)
{
	mustClose = false;
	
	notificationLabel->setText(message.c_str());
    notificationLabel->raise();
	QFontMetrics fm = notificationLabel->fontMetrics();
	int width = fm.width(message.c_str());
	int height = fm.height();
		
	notificationLabel->move(400 - (width / 2) - 20, 400  - (height / 2) - 10);
	notificationLabel->resize(width + 40, height + 20);   
	notificationLabel->show();
		
	if (timer != nullptr)
	{
		if (!timer->isActive())
		{
			anim->setStartValue(0);
			anim->setEndValue(1);
			anim->start();
		}
		
		timer->stop();
	}
	else
	{
		anim->setStartValue(0);
		anim->setEndValue(1);
		anim->start();
	}
	
	timer = new QTimer();
	timer->setSingleShot(true);
	timer->start(duration);
			
	connect(timer, SIGNAL(timeout()), this, SLOT(Close()));
}
