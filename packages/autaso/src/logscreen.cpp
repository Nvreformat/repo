#include "logscreen.h"
#include <map>
#include <string>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <time.h>
#include <stdarg.h>

using namespace std;

string GetFormattedDate()
{
	string ret;
	
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);

	stringstream ss;
	ss << "[" << setw(2) << setfill('0') << to_string(tm.tm_hour) << ":" << setw(2) << setfill('0') << to_string(tm.tm_min) << ":" << setw(2) << setfill('0') << to_string(tm.tm_sec) << "]";
	ret = ss.str();

	return ret;
}

struct Tab
{
	QWidget *tab;
    QTextEdit *textEdit;
    string name;
	
	Tab(string name)
	{
		this->name = name;
		tab = new QWidget();
        textEdit = new QTextEdit(tab);
        textEdit->setGeometry(QRect(0, 0, 801, 391));
        textEdit->setStyleSheet(QLatin1String("QTextEdit\n"
			"{\n"
			"color: white;\n"
			"background: black;\n"
			"font-size: 18px;\n"
			"}"));
        textEdit->setReadOnly(true);
        textEdit->setTextInteractionFlags(Qt::NoTextInteraction);
        logScreen->ui.tabManager->addTab(tab, name.c_str());
	}
	
	void Print(string message, int level)
	{
		string newLine;
		
		if (textEdit->toPlainText() != "")
			newLine = "\n";
		else
			newLine = "";
			
		if (level == LOG_NORMAL)
			textEdit->setTextColor(QColor(255, 255, 255));
		else if (level == LOG_WARNING)
			textEdit->setTextColor(QColor(255, 153, 0));
		else if (level == LOG_ERROR)
			textEdit->setTextColor(QColor(255, 0, 0));
		
		textEdit->insertPlainText((newLine + GetFormattedDate() + " " + message).c_str());
		textEdit->verticalScrollBar()->setValue(textEdit->verticalScrollBar()->maximum());
	}

};

map<string, Tab*> stringToTab; 

void Log(string tag, string message, int level)
{
	Tab* tab = stringToTab[tag];
	
	if (tab == nullptr && tag != "All")
	{
		tab = new Tab(tag);
		stringToTab[tag] = tab;
	}
	
	string newLine;
		
	if (logScreen->ui.textEdit->toPlainText() != "")
		newLine = "\n";
	else
		newLine = "";
	
	if (level == LOG_NORMAL)
		logScreen->ui.textEdit->setTextColor(QColor(255, 255, 255));
	else if (level == LOG_WARNING)
		logScreen->ui.textEdit->setTextColor(QColor(255, 153, 0));
	else if (level == LOG_ERROR)
		logScreen->ui.textEdit->setTextColor(QColor(255, 0, 0));
		
	logScreen->ui.textEdit->insertPlainText((newLine + GetFormattedDate() + string(" [") + tag + "] " + message).c_str());
	logScreen->ui.textEdit->verticalScrollBar()->setValue(logScreen->ui.textEdit->verticalScrollBar()->maximum());
	cout << (GetFormattedDate() + string(" [") + tag + "] " + message).c_str() << endl;
	
	if (tag != "All")
		tab->Print(message, level);
}

string string_format(string fmt, va_list ap) {
    int size = ((int)fmt.size()) * 2 + 50;   // Use a rubric appropriate for your code
    string str;
    
    while (1) {     // Maximum two passes on a POSIX system...
        str.resize(size);
        //va_start(ap, fmt);
        int n = vsnprintf((char *)str.data(), size, fmt.c_str(), ap);
        //va_end(ap);
        if (n > -1 && n < size) {  // Everything worked
            str.resize(n);
            return str;
        }
        if (n > -1)  // Needed size returned
            size = n + 1;   // For null char
        else
            size *= 2;      // Guess at a larger size (OS specific)
    }
    return str;
}

void LogI(std::string tag, std::string format, ...)
{
	va_list args;
    va_start(args, format);
    
    string message = string_format(string(format), args);
    
    va_end(args);
    
    Log(tag, message, LOG_NORMAL);
}

void LogW(std::string tag, std::string format, ...)
{
	va_list args;
    va_start(args, format);
    
    string message = string_format(string(format), args);
    
    va_end(args);
    
    Log(tag, message, LOG_WARNING);
}

void LogE(std::string tag, std::string format, ...)
{
	va_list args;
    va_start(args, format);
    
    string message = string_format(string(format), args);
    
    va_end(args);
    
    Log(tag, message, LOG_ERROR);
}


LogScreen::LogScreen()
{
	QWidget *widget = new QWidget;
		
	ui.setupUi(widget);

	Setup(widget);
	
	ui.tabManager->tabBar()->setExpanding(true);
	QScroller::grabGesture(ui.textEdit, QScroller::LeftMouseButtonGesture);
}
