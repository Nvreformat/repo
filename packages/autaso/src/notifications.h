#pragma once

#include <string>
#include <QApplication>
#include <QtUiTools>

class Notifications : public QObject
{
	Q_OBJECT
	
	QLabel* notificationLabel;
	QTimer* timer = nullptr;
	QPropertyAnimation* anim;
	bool mustClose = false;
	
	public:
		Notifications();
		void Put(std::string message) { Put(message, 1500); }
		void Put(std::string message, int duration);
		
	private slots:
		void AnimFinished();
		void Close();
};

extern Notifications* notifications;
