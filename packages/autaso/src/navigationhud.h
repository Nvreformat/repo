#pragma once
#include <QApplication>
#include <QtUiTools>
#include <QWidget>
#include "ui_hudtop.h"
#include "ui_hudbottom.h"
#include <string>
#include "mpd.h"

class NavigationHud : public QObject
{
	Q_OBJECT
	
	Ui::HudTopForm uiTop;
	Ui::HudBottomForm uiBottom;
	
	QPropertyAnimation* animation;
	QWidget* widgetTop;
	QWidget* widgetBottom;
	QIcon playIcon;
	QIcon pauseIcon;
	bool bottomBarVisible = false;
	
	public:
		NavigationHud();
		void SetBottomBarVisible(bool);
		bool IsBottomBarVisible() { return bottomBarVisible; }
		
	private slots:
		void OnBackClick();
		void OnTimerTick();
		void OnTogglePauseClick();
		void OnNextClick();
		void OnPreviousClick();
		void OnVolupClick();
		void OnVoldownClick();
};
