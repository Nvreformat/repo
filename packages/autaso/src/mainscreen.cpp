#include "mainscreen.h"
#include "musicscreen.h"
#include "mapsscreen.h"
#include "logscreen.h"

void MainScreen::OnMusicClick()
{
	musicScreen->SwitchTo();
}

void MainScreen::OnMapsClick()
{
	mapsScreen->SwitchTo();
}

void MainScreen::OnLogClick()
{
	logScreen->SwitchTo();
}

MainScreen::MainScreen()
{
	QWidget* widget = new QWidget;	
	ui.setupUi(widget);	
	Setup(widget);

	this->connect(ui.MusicButton, SIGNAL(clicked()), this, SLOT(OnMusicClick()));
	this->connect(ui.MapsButton, SIGNAL(clicked()), this, SLOT(OnMapsClick()));
	this->connect(ui.LogButton, SIGNAL(clicked()), this, SLOT(OnLogClick()));
}
