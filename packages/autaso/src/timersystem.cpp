#include "timersystem.h"
#include <QApplication>
#include <QtUiTools>
#include <QWidget>
#include <vector>

namespace TimerSystem
{
	struct TimerData
	{
		std::function<void(void)> function;
		int cycle;
		int current = 0;
	};
	
	std::vector<TimerData> functions;
	
	void ExecuteCallbacks()
	{
		for (TimerData& data : functions)
		{
			data.current++;
			
			if (data.current % data.cycle == 0)
				data.function();
		}
		
		QTimer::singleShot(250, &ExecuteCallbacks);
	}
	
	void Start()
	{
		QTimer::singleShot(250, &ExecuteCallbacks);
	}
	
	void AddTimer(std::function<void(void)> function, int priority)
	{
		TimerData data;
		
		data.cycle = priority;
		data.function = function;
		
		functions.push_back(data);
	}
}
