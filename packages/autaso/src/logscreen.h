#pragma once
#include "basescreen.h"
#include "ui_systemlog.h"
#include <string>

class LogScreen : public BaseScreen
{
	Q_OBJECT
	
	public:
		Ui::LogsForm ui;
		
		LogScreen();
};

#define LOG_NORMAL 0
#define LOG_WARNING 1
#define LOG_ERROR 2

void LogI(std::string tag, std::string format, ...);
void LogW(std::string tag, std::string format, ...);
void LogE(std::string tag, std::string format, ...);

