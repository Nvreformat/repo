#pragma once
#include "basescreen.h"
#include "ui_main.h"

class MainScreen : public BaseScreen
{
	Q_OBJECT
	Ui::MainForm ui;
	
	public:
		MainScreen();
		
	private slots:
		void OnMusicClick();
		void OnMapsClick();
		void OnLogClick();
};
