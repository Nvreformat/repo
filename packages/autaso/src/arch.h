#pragma once

#if __SIZEOF_POINTER__ == 8
    #define DESKTOP
#else
    #define ARM
#endif

#undef ARM
#define DESKTOP
