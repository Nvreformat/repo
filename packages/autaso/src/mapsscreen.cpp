#include "mapsscreen.h"
#include "timersystem.h"

void MapsScreen::InitGPS()
{
	int rc = gps_open("localhost", "2947", &gpsData);
		
	if (rc == -1)
		LogE("GPS", "Failed to start GPS service, error code: %i (%s)", rc, gps_errstr(rc));
	else
	{
		gps_stream(&gpsData, WATCH_ENABLE | WATCH_JSON, NULL);
			
		LogI("GPS", "Started GPS service");
		gpsOK = true;
	}
}

MapsScreen::MapsScreen()
{
	QWidget *widget = new QWidget;

	ui.setupUi(widget);
	ui.webView->setAttribute(Qt::WA_AcceptTouchEvents, false);
	ui.webView->load(QUrl("qrc:/misc/maps/maps.html"));
	ui.webView->setZoomFactor(1.35);
		
	TimerSystem::AddTimer(std::bind(&MapsScreen::OnTimerTick, this), 8);
		
	Setup(widget);
}

void MapsScreen::OnTimerTick()
{
	if (!gpsOK)
		return;
		
	int rc;
				
	if ((rc = gps_read(&gpsData)) == -1)
	{
		LogE("GPS", "Error occured reading gps data. code: %i, reason: %s", rc, gps_errstr(rc));
	}
	else
	{
		if ((gpsData.status == STATUS_FIX) && (gpsData.fix.mode == MODE_2D || gpsData.fix.mode == MODE_3D) && !isnan(gpsData.fix.latitude) && !isnan(gpsData.fix.longitude))
		{
			LogI("GPS", "Latitude: %f, Longitude: %f", gpsData.fix.latitude, gpsData.fix.longitude);
			ui.webView->page()->mainFrame()->evaluateJavaScript((std::string("addMarker(") + std::to_string(gpsData.fix.latitude) + ", " + std::to_string(gpsData.fix.longitude) + ");").c_str());
		}
	}
}
